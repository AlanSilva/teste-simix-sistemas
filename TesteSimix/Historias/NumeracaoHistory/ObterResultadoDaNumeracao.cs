﻿using System.Collections.Generic;

namespace Historias.NumeracaoHistory
{
    public class ObterResultadoDaNumeracao
    {
        public List<string> Executar()
        {
            var resultadoDaNumeracao = new List<string>();

            var quantidadeDeNumeros = 200;

            for (int indice = 1; indice <= quantidadeDeNumeros; indice++)
            {
                bool numeroEhDivisivelPor3 = VerificarSeIndiceEhDivisivelPor3(indice);

                bool numeroEhDivisivelPor5 = VerificarSeIndiceEhDivisivelPor5(indice);

                if (numeroEhDivisivelPor3 && numeroEhDivisivelPor5)
                {
                    resultadoDaNumeracao.Add($"{ indice } &rArr; <strong class='text-danger'>Z</strong>");
                }
                else if (numeroEhDivisivelPor3)
                {
                    resultadoDaNumeracao.Add($"{ indice } &rArr; <strong class='text-primary'>X</strong>");
                }
                else if (numeroEhDivisivelPor5)
                {
                    resultadoDaNumeracao.Add($"{ indice } &rArr; <strong class='text-success'>Y</strong>");
                }
                else
                {
                    resultadoDaNumeracao.Add($"{ indice } &rArr; <strong>{ indice }</strong>");
                }
            }

            return resultadoDaNumeracao;
        }

        private static bool VerificarSeIndiceEhDivisivelPor3(int indice)
        {
            bool ehDivisivel = indice % 3 == 0;

            return ehDivisivel;
        }

        private static bool VerificarSeIndiceEhDivisivelPor5(int indice)
        {
            bool ehDivisivel = indice % 5 == 0;

            return ehDivisivel;
        }
    }
}
