﻿using Historias.NumeracaoHistory;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using Web.Models;

namespace Web.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index([FromServices] ObterResultadoDaNumeracao obterResultadoDaNumeracao)
        {
            var resultadoDaNumeracao = obterResultadoDaNumeracao.Executar();

            return View(resultadoDaNumeracao);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
